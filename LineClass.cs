using System;

namespace Line
{
    class LineClass
    {
        private int length;
        private char character;

        public int Length { get => length; set => length = value; }
        public char Character { get => character; set => character = value; }

        public void Draw(){
            for (int i = 0; i < Length; i++)
            {
                System.Console.Write(Character);
            }
            System.Console.WriteLine();
        }
    }
}